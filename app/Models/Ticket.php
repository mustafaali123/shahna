<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Ticket
 * 
 * @property int $id
 * @property int $code
 * @property int $user_id
 * @property string $subject
 * @property string|null $details
 * @property string|null $files
 * @property string $status
 * @property int $viewed
 * @property int $client_viewed
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class Ticket extends Model
{
	protected $table = 'tickets';

	protected $casts = [
		'code' => 'int',
		'user_id' => 'int',
		'viewed' => 'int',
		'client_viewed' => 'int'
	];

	protected $fillable = [
		'code',
		'user_id',
		'subject',
		'details',
		'files',
		'status',
		'viewed',
		'client_viewed'
	];
}

@extends('frontend.layouts.request')

@section('style')
<link rel="stylesheet" href="{{ static_asset('assets/front/css/pages/shipmentdate.css')}}" />
@endsection
@section('content')

    <div class="welcome-to-you">
      <div class="container">
        <div class="content">
          <div class="row">
            <div class="details col-md-6">
              <h2 class="main-title h1">{{ website_info('shipment_header_'.app()->getLocale()) }}</h2>
              <p class="text">{{ website_info('shipment_text_'.app()->getLocale()) }}</p>
            </div>
            <div class="photo col-md-6">
              <img
                class="smart-phone w-100"
                src="{{static_asset('websites/'.website_info('shipment_image'))}}"
                alt=""
              />
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="your-orders">
        <div class="container">
          <div class="details">
            <div class="every-user d-flex">
              <div class="user-img rounded-circle">
                <img
                  class="photo w-100 h-100 rounded-circle"
                  src="{{ static_asset('assets/front/images/home/08.png') }}"
                  alt="user-img"
                />
              </div>
              <div class="user-name">
                <span class="welcome">{{ translate('Welcome') }}  </span>
                <h6 class="name">{{ auth()->user()->name }} </h6>
              </div>
            </div>
            <div class="date d-flex">
              <span
                class="date-icon d-flex justify-content-center align-items-center"
              >
                <i class="far fa-calendar-check"></i>
              </span>
              <p class="text">{{ translate('All your Request in our site') }}</p>
            </div>
          </div>
            @foreach ($shipments as $shipment)
            <div class="orders-details">
                <div class=" every-order  d-flex justify-content-between  align-items-center">
                    <div class="details-ship d-flex">
                    <span class="box-icon d-flex justify-content-center align-items-center">
                        <span
                        class="one d-flex justify-content-center align-items-center"
                        ><i class="fas fa-box"></i
                        ></span>
                    </span>
                    <div class="about-order">
                        <h6 class="address"> {{ translate('From') }} {{  $shipment->from_country->name  }} {{ translate('To') }} {{ $shipment->to_country->name }} </h6>
                        <div class="dates">
                        <div class="texts d-flex">
                            <p class="part">
                            <span class="part-1"> {{ translate('Requested In') }} : </span
                            ><span class="part-2"> {{ $shipment->created_at }} </span>
                            </p>
                            <p class="part">
                            <span class="part-1"> {{ translate('Deliver At') }} : </span
                            ><span class="part-2"> {{ $shipment->shipping_date }}</span>
                            </p>
                        </div>
                        </div>
                    </div>
                    </div>
                    <div class="success">
                    <span class="one">{{ $shipment->getStatus() }} </span>
                    <span class="two rounded-circle"
                        ><i class="fas fa-check"></i
                    ></span>
                    </div>
                </div>
            </div>
            @endforeach
          {{-- <div class="orders-details">
            <div
              class="
                every-order
                d-flex
                justify-content-between
                align-items-center
              "
            >
              <div class="details-ship d-flex">
                <span
                  class="
                    box-icon
                    d-flex
                    justify-content-center
                    align-items-center
                  "
                >
                  <span
                    class="one d-flex justify-content-center align-items-center"
                    ><i class="fas fa-box"></i
                  ></span>
                </span>
                <div class="about-order">
                  <h6 class="address">شحن طرد من مصر الي السعودية</h6>
                  <div class="dates">
                    <div class="texts d-flex">
                      <p class="part">
                        <span class="part-1">طلب في : </span
                        ><span class="part-2">20 /9 / 2021</span>
                      </p>
                      <p class="part">
                        <span class="part-1">وصل في: </span
                        ><span class="part-2">27 /9 / 2021</span>
                      </p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="success">
                <span class="one false">عملة غير مكتملة</span>
                <span class="two rounded-circle false"
                  ><i class="fas fa-check"></i
                ></span>
              </div>
            </div>
          </div> --}}
        </div>
      </div>
<br>
<br>
<br>
      @endsection

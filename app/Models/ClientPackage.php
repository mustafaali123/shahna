<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ClientPackage
 * 
 * @property int $id
 * @property int $client_id
 * @property int $package_id
 * @property float $package_cost
 * @property string|null $package_name
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @package App\Models
 */
class ClientPackage extends Model
{
	protected $table = 'client_package';

	protected $casts = [
		'client_id' => 'int',
		'package_id' => 'int',
		'package_cost' => 'float'
	];

	protected $fillable = [
		'client_id',
		'package_id',
		'package_cost',
		'package_name'
	];
}

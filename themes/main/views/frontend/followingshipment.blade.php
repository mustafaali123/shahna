@extends('frontend.layouts.request')

@section('content')

    <div class="welcome-to-you">
      <div class="container">
        <div class="content">
          <div class="row">
            <div class="details col-md-6">
              <h2 class="main-title h1">{{ website_info('follow_header_'.app()->getLocale()) }}</h2>
              <p class="text">{{ website_info('follow_text_'.app()->getLocale()) }}</p>
            </div>
            <div class="photo col-md-6">
              <img
                class="smart-phone w-100"
                src="{{static_asset('websites/'.website_info('follow_image'))}}"
                alt=""
              />
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="shipmentorder text-center">
      <p class="text">{{  translate('You can easily order a shipment from anywhere in the world and it will be delivered') }}</p>
      <p class="text">{{ translate('As quickly and safely as possible') }}</p>
      <div class="follow-shipment">

        <form class="form" action="{{route('shipmentsclient.tracking')}}" method="GET">
            <button class="shipment-btn" type="submit">{{ translate('track your shipment') }}</button>
            <input class="shipment-input" type="text" name="code" placeholder="{{translate('Example: SH00001')}}">
        </form>

      </div>
    </div>
    @if($shipment)
    <section class="shipment-status">
        <h2 class="text-center title">حالة شحنتك</h2>
        <div class="container">
          <div class="tabs d-flex justify-content-around">
            <button
              class="tablinks active"
              onclick="openCity(event, 'content-1')"
            >
              <i class="fas fa-check"></i>
            </button>
            <button class="tablinks" onclick="openCity(event, 'content-2')">
              <i class="fas fa-check"></i>
            </button>
            <button class="tablinks" onclick="openCity(event, 'content-3')">
              <i class="fas fa-check"></i>
            </button>
          </div>

          <div class="content">
            <div id="content-1" class="tabcontent">
              <div class="details">
                <h6>{{$shipment->getStatus()}} </h6>
                <p>  {{translate('Created')}} {{$shipment->created_at->diffForHumans()}}</p>
              </div>
              <div class="details">
                <h6>{{ translate('Shipping Adress') }} </h6>
                <p>{{ $shipment->client_address }}</p>
              </div>
              <div class="details">
                <h6>{{ translate('Mobile') }}</h6>
                <p>{{ $shipment->client_phone }}</p>
              </div>
              <div class="details">
                <h6>{{ translate('Deiver at') }}</h6>
                <p>10/6/2021</p>
              </div>
              <div class="details">
                <h6>{{translate('delivery address')}}</h6>
                <p>{{ $shipment->reciver_address }}</p>
              </div>
              <div class="details">
                <h6>{{ translate('Payment Type') }}</h6>
                <p>{{ $shipment->getPaymentType() }} </p>
              </div>
            </div>

            {{-- <div id="content-2" class="tabcontent">
              <div class="details">
                <h6>طرد مغلق</h6>
                <p>تم الطلب في 9/30/2021</p>
              </div>
              <div class="details">
                <h6>عنوان الشحن</h6>
                <p>10 شارع الملك فهد - العتبة - مصر</p>
              </div>
              <div class="details">
                <h6>رقم الجوّال</h6>
                <p>+20565984566</p>
              </div>
              <div class="details">
                <h6>من المقرر استلام الطرد في</h6>
                <p>10/6/2021</p>
              </div>
              <div class="details">
                <h6>مكان التسليم</h6>
                <p>المنزل</p>
              </div>
              <div class="details">
                <h6>طريقة الدفع</h6>
                <p>عند الاستلام</p>
              </div>
            </div>

            <div id="content-3" class="tabcontent">
              <div class="details">
                <h6>طرد مغلق</h6>
                <p>تم الطلب في 9/30/2021</p>
              </div>
              <div class="details">
                <h6>عنوان الشحن</h6>
                <p>10 شارع الملك فهد - العتبة - مصر</p>
              </div>
              <div class="details">
                <h6>رقم الجوّال</h6>
                <p>+20565984566</p>
              </div>
              <div class="details">
                <h6>من المقرر استلام الطرد في</h6>
                <p>10/6/2021</p>
              </div>
              <div class="details">
                <h6>مكان التسليم</h6>
                <p>المنزل</p>
              </div>
              <div class="details">
                <h6>طريقة الدفع</h6>
                <p>عند الاستلام</p>
              </div>
            </div> --}}
          </div>

          <p class="text-center">
            يمكنك استرجاع الطرد في حالة ان وصلك به عيوب توصيل مثل كسور او خدوش او
            أي شئ له علاقة بخدمة التوصيل
          </p>
        </div>
    </section>
    @endif
@endsection

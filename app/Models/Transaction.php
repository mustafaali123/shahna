<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Transaction
 * 
 * @property int $id
 * @property float $value
 * @property int $transaction_owner
 * @property int|null $captain_id
 * @property int|null $client_id
 * @property int|null $branch_id
 * @property int $type
 * @property int|null $shipment_id
 * @property int|null $mission_id
 * @property string|null $description
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @package App\Models
 */
class Transaction extends Model
{
	protected $table = 'transactions';

	protected $casts = [
		'value' => 'float',
		'transaction_owner' => 'int',
		'captain_id' => 'int',
		'client_id' => 'int',
		'branch_id' => 'int',
		'type' => 'int',
		'shipment_id' => 'int',
		'mission_id' => 'int'
	];

	protected $fillable = [
		'value',
		'transaction_owner',
		'captain_id',
		'client_id',
		'branch_id',
		'type',
		'shipment_id',
		'mission_id',
		'description'
	];
}

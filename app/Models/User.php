<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class User
 * 
 * @property int $id
 * @property int|null $referred_by
 * @property string|null $provider_id
 * @property string $user_type
 * @property string $name
 * @property string|null $email
 * @property Carbon|null $email_verified_at
 * @property string|null $verification_code
 * @property string|null $new_email_verificiation_code
 * @property string|null $password
 * @property string|null $api_token
 * @property string|null $remember_token
 * @property string|null $avatar
 * @property string|null $avatar_original
 * @property string|null $address
 * @property string|null $country
 * @property string|null $city
 * @property string|null $postal_code
 * @property string|null $phone
 * @property float $balance
 * @property int $banned
 * @property string|null $referral_code
 * @property int|null $customer_package_id
 * @property int|null $remaining_uploads
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property int $role_id
 *
 * @package App\Models
 */
class User extends Model
{
	protected $table = 'users';

	protected $casts = [
		'referred_by' => 'int',
		'balance' => 'float',
		'banned' => 'int',
		'customer_package_id' => 'int',
		'remaining_uploads' => 'int',
		'role_id' => 'int'
	];

	protected $dates = [
		'email_verified_at'
	];

	protected $hidden = [
		'password',
		'api_token',
		'remember_token'
	];

	protected $fillable = [
		'referred_by',
		'provider_id',
		'user_type',
		'name',
		'email',
		'email_verified_at',
		'verification_code',
		'new_email_verificiation_code',
		'password',
		'api_token',
		'remember_token',
		'avatar',
		'avatar_original',
		'address',
		'country',
		'city',
		'postal_code',
		'phone',
		'balance',
		'banned',
		'referral_code',
		'customer_package_id',
		'remaining_uploads',
		'role_id'
	];
}

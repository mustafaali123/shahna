<!DOCTYPE html>
<html lang="ar" dir="rtl"> 
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="libs/bootstrap/css/bootstrap.rtl.min.css" />
    <link rel="stylesheet" href="libs/fontawesome/css/all.css" />
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,400;0,700;1,500&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="css/pages/signup.css" />
    <title> New Password</title>
  </head>
  <body>
    <section class="register">
      <div class="container">
        <!-- Start Header -->
        <header class="registration-header d-flex justify-content-between align-items-center">
          <div class="languages">
            <span class="active">عربي</span>
            <span>English</span>
          </div>
          <div class="logo-container">
            <a href="index.html"><img class="logo"  src="./images/logo.png" alt="logo"></a>
          </div>
        </header>
        <!-- End Header -->

        <!-- Start Sign  Form -->
        <form action="" class="sign-form new-password">
          <h3 class="title text-center">كلمة مرور جديدة</h3>
          <p class="text text-center">قم بتغير كلمة مرورك  علي منصة شحنة</p>
          <div class="form-group">
            <div class="details">
              <label class="form-label" for="">كلمة المرور</label>
              <div class="input-container">
                <input class="form-input different" type="password">
                <i class="fas fa-lock icon"></i>
              </div>
            </div>
            <div class="details">
              <label class="form-label" for="">تأكيد كلمة المرور الجديدة</label>
              <div class="input-container">
                <input class="form-input different" type="password">
                <i class="fas fa-lock icon"></i>
              </div>
            </div>
            
          </div>
          <button class="submit-button"><a class="link d-block" href="successpassword.html">ارسال</a></button>
        </form>
        <!-- End Sign  Form -->
      </div>
    </section>
  </body>
</html>

<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Policy
 * 
 * @property int $id
 * @property string $name
 * @property string|null $content
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class Policy extends Model
{
	protected $table = 'policies';

	protected $fillable = [
		'name',
		'content'
	];
}

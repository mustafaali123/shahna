<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class AdminMenuItem
 * 
 * @property int $id
 * @property string $label
 * @property string $link
 * @property int $parent
 * @property int $sort
 * @property string|null $class
 * @property int $menu
 * @property int $depth
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * 
 * @property AdminMenu $admin_menu
 *
 * @package App\Models
 */
class AdminMenuItem extends Model
{
	protected $table = 'admin_menu_items';

	protected $casts = [
		'parent' => 'int',
		'sort' => 'int',
		'menu' => 'int',
		'depth' => 'int'
	];

	protected $fillable = [
		'label',
		'link',
		'parent',
		'sort',
		'class',
		'menu',
		'depth'
	];

	public function admin_menu()
	{
		return $this->belongsTo(AdminMenu::class, 'menu');
	}
}

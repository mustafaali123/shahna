<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ShipmentSetting
 * 
 * @property int $id
 * @property string $key
 * @property string $value
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @package App\Models
 */
class ShipmentSetting extends Model
{
	protected $table = 'shipment_settings';

	protected $fillable = [
		'key',
		'value'
	];
}

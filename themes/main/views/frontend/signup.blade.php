<!DOCTYPE html>
<html lang="ar" dir="rtl"> 
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="libs/bootstrap/css/bootstrap.rtl.min.css" />
    <link rel="stylesheet" href="libs/fontawesome/css/all.css" />
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,400;0,700;1,500&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="css/pages/signup.css" />
    <title>Sign up</title>
  </head>
  <body>
    <section class="register">
      <div class="container">
        <header class="registration-header d-flex justify-content-between align-items-center">
          <div class="languages">
            <span class="active">عربي</span>
            <span>English</span>
          </div>
          <div class="logo-container">
            <a href="index.html"><img class="logo"  src="./images/logo.png" alt="logo"></a>
          </div>
        </header>

        <!-- Start Sign Form -->
        <form action="" class="sign-form">
          <h3 class="title text-center">انشاء حساب</h3>
          <div class="form-group full-name"><!-- Start Full name -->
            <div class="details full-name">
              <label class="form-label" for="">الاسم الاول</label>
              <div class="input-container">
                <input class="form-input" type="text">
                <i class="fas fa-user icon"></i>
              </div>
            </div>
            <div class="details full-name">
              <label class="form-label" for="">الاسم الاخير</label>
              <div class="input-container">
                <input class="form-input" type="text">
                <i class="fas fa-user icon"></i>
              </div>
            </div>
          </div><!-- End Full name -->
          <div class="form-group"><!-- Start Email -->
            <div class="details">
              <label class="form-label" for="">البريد الالكتروني</label>
              <div class="input-container">
                <input class="form-input different" type="email">
                <i class="far fa-envelope icon"></i>
              </div>
            </div>
          </div><!-- End Email -->
          <div class="form-group"><!-- Start password -->
            <div class="details">
              <label class="form-label" for="">كلمة المرور</label>
              <div class="input-container">
                <input class="form-input different" type="password">
                <i class="fas fa-lock icon"></i>
              </div>
            </div>
          </div><!-- End password -->
          <button class="submit-button">انشاء حساب</button>
        </form>
        <!-- End Sign Form -->

        <div class="text-center account">
          <span>هل لديك حساب الفعل ؟</span>
          <span ><a class="go-to-sign" href="signin.html">سجل دخولك</a></span>
        </div>

      </div>
    </section>
  </body>
</html>

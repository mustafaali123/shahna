<!DOCTYPE html>
<html dir="rtl" lang="ar">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link href="{{ static_asset('assets/dashboard/plugins/global/plugins.bundle.rtl.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{ static_asset('assets/front/libs/bootstrap/css/bootstrap.rtl.min.css') }}" />
    <link rel="stylesheet" href="{{ static_asset('assets/front/libs/fontawesome/css/all.css')}}" />
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,400;0,700;1,500&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{ static_asset('assets/front/css/pages/main.css')}}" />
    <link rel="stylesheet" href="{{ static_asset('assets/front/css/pages/signup.css')}}" />
    <link rel="stylesheet" href="{{ static_asset('assets/front/css/pages/followingshipment.css')}}" />
    <title>Shahna</title>
</head>

<body>
    @yield('content')
        <script src="{{ static_asset('assets/front/libs/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
</body>

</html>

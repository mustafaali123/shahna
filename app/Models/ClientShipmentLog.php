<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ClientShipmentLog
 * 
 * @property int $id
 * @property int $from
 * @property int $to
 * @property int $created_by
 * @property int $shipment_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @package App\Models
 */
class ClientShipmentLog extends Model
{
	protected $table = 'client_shipment_logs';

	protected $casts = [
		'from' => 'int',
		'to' => 'int',
		'created_by' => 'int',
		'shipment_id' => 'int'
	];

	protected $fillable = [
		'from',
		'to',
		'created_by',
		'shipment_id'
	];
}

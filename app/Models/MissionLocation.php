<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class MissionLocation
 * 
 * @property int $id
 * @property string|null $street_address_map
 * @property string|null $lat
 * @property string|null $lng
 * @property string|null $url
 * @property int|null $mission_id
 * @property int $mission_status_id
 * @property int|null $shipment_id
 * @property int $shipment_status_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @package App\Models
 */
class MissionLocation extends Model
{
	protected $table = 'mission_locations';

	protected $casts = [
		'mission_id' => 'int',
		'mission_status_id' => 'int',
		'shipment_id' => 'int',
		'shipment_status_id' => 'int'
	];

	protected $fillable = [
		'street_address_map',
		'lat',
		'lng',
		'url',
		'mission_id',
		'mission_status_id',
		'shipment_id',
		'shipment_status_id'
	];
}

<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class AppSetting
 * 
 * @property int $id
 * @property string|null $name
 * @property string|null $logo
 * @property int|null $currency_id
 * @property string|null $currency_format
 * @property string|null $facebook
 * @property string|null $twitter
 * @property string|null $instagram
 * @property string|null $youtube
 * @property string|null $google_plus
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @package App\Models
 */
class AppSetting extends Model
{
	protected $table = 'app_settings';

	protected $casts = [
		'currency_id' => 'int'
	];

	protected $fillable = [
		'name',
		'logo',
		'currency_id',
		'currency_format',
		'facebook',
		'twitter',
		'instagram',
		'youtube',
		'google_plus'
	];
}

<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class LbBlock
 * 
 * @property int $id
 * @property string|null $raw_title
 * @property string|null $raw_content
 * @property string|null $rendered_content
 * @property string $status
 * @property string $slug
 * @property string $type
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @package App\Models
 */
class LbBlock extends Model
{
	protected $table = 'lb_blocks';

	protected $fillable = [
		'raw_title',
		'raw_content',
		'rendered_content',
		'status',
		'slug',
		'type'
	];
}

<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Upload
 * 
 * @property int $id
 * @property string|null $file_original_name
 * @property string|null $file_name
 * @property int|null $user_id
 * @property int|null $file_size
 * @property string|null $extension
 * @property string|null $type
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property string|null $deleted_at
 *
 * @package App\Models
 */
class Upload extends Model
{
	use SoftDeletes;
	protected $table = 'uploads';

	protected $casts = [
		'user_id' => 'int',
		'file_size' => 'int'
	];

	protected $fillable = [
		'file_original_name',
		'file_name',
		'user_id',
		'file_size',
		'extension',
		'type'
	];
}

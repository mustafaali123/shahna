<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ShipmentReason
 * 
 * @property int $id
 * @property int|null $shipment_id
 * @property int|null $reason_id
 * @property string|null $type
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @package App\Models
 */
class ShipmentReason extends Model
{
	protected $table = 'shipment_reasons';

	protected $casts = [
		'shipment_id' => 'int',
		'reason_id' => 'int'
	];

	protected $fillable = [
		'shipment_id',
		'reason_id',
		'type'
	];
}

<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class AdminWidget
 * 
 * @property int $id
 * @property string|null $title
 * @property string|null $name
 * @property string|null $type
 * @property string|null $object
 * @property string|null $value
 * @property string|null $link
 * @property string $lang
 * @property int $count
 * @property int $parent_id
 * @property string|null $class
 * @property string|null $widget_frontend
 * @property string|null $widget_backend
 * @property string|null $container_widget_backend
 * @property string|null $update
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @package App\Models
 */
class AdminWidget extends Model
{
	protected $table = 'admin_widgets';

	protected $casts = [
		'count' => 'int',
		'parent_id' => 'int'
	];

	protected $fillable = [
		'title',
		'name',
		'type',
		'object',
		'value',
		'link',
		'lang',
		'count',
		'parent_id',
		'class',
		'widget_frontend',
		'widget_backend',
		'container_widget_backend',
		'update'
	];
}

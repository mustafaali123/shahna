<?php

namespace App\Http\Controllers;

use App\Website;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class WebsiteController extends Controller
{
    public function index(){
        $data=Website::all();
        return view('backend.website.index',compact('data'));
    }
    public function store(Request $request)
    {
        try{
            $input=$request->all();
            unset($input['_token']);

            foreach($input as $key=>$val){
                $info=Website::where('option',$key)->first();
                if(request()->hasFile($key)) {
                    $file = request()->file($key);
                    if ($file) {
                        if($info){
                            File::delete('websites/' . $info->value);
                        }
                        $fileName = time() . rand(0, 999999999) . '.' . $file->getClientOriginalExtension();
                        $file->storeAs('websites', $fileName);
                        $item['value'] = $fileName;
                    }
                }else{
                    $item['value']=$val;
                }
                if($info){
                    $info->update($item);
                }
             }
                flash(translate('data saveed successfully'))->success();
                return back();
        } catch (\Exception $e) {
            DB::rollback();
            flash(translate('Someting went error'))->error();
            return back();
        }

    }

}

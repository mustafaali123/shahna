<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Mission
 * 
 * @property int $id
 * @property string $code
 * @property int $type
 * @property float $amount
 * @property int $captain_id
 * @property string|null $address
 * @property int|null $state
 * @property int|null $area
 * @property int $order
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property int $status_id
 * @property int $client_id
 * @property string|null $due_date
 * @property int|null $to_branch_id
 * @property string|null $seg_img
 * @property string|null $otp
 *
 * @package App\Models
 */
class Mission extends Model
{
	protected $table = 'missions';

	protected $casts = [
		'type' => 'int',
		'amount' => 'float',
		'captain_id' => 'int',
		'state' => 'int',
		'area' => 'int',
		'order' => 'int',
		'status_id' => 'int',
		'client_id' => 'int',
		'to_branch_id' => 'int'
	];

	protected $fillable = [
		'code',
		'type',
		'amount',
		'captain_id',
		'address',
		'state',
		'area',
		'order',
		'status_id',
		'client_id',
		'due_date',
		'to_branch_id',
		'seg_img',
		'otp'
	];
}

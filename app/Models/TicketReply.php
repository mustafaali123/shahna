<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TicketReply
 * 
 * @property int $id
 * @property int $ticket_id
 * @property int $user_id
 * @property string $reply
 * @property string|null $files
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class TicketReply extends Model
{
	protected $table = 'ticket_replies';

	protected $casts = [
		'ticket_id' => 'int',
		'user_id' => 'int'
	];

	protected $fillable = [
		'ticket_id',
		'user_id',
		'reply',
		'files'
	];
}

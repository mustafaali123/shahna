<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class BusinessSetting
 * 
 * @property int $id
 * @property string $type
 * @property string|null $value
 * @property string|null $key
 * @property string|null $lang
 * @property string|null $name
 * @property Carbon $created_at
 * @property Carbon|null $updated_at
 *
 * @package App\Models
 */
class BusinessSetting extends Model
{
	protected $table = 'business_settings';

	protected $fillable = [
		'type',
		'value',
		'key',
		'lang',
		'name'
	];
}

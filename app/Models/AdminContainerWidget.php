<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class AdminContainerWidget
 * 
 * @property int $id
 * @property int|null $widget_id
 * @property int|null $container_id
 * @property string $title
 * @property string|null $name
 * @property string|null $type
 * @property string|null $object
 * @property string|null $value
 * @property string|null $link
 * @property string $lang
 * @property int $count
 * @property int $sort
 * @property string|null $class
 * @property string|null $widget_frontend
 * @property string|null $widget_backend
 * @property string|null $container_widget_backend
 * @property string|null $update
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @package App\Models
 */
class AdminContainerWidget extends Model
{
	protected $table = 'admin_container_widgets';

	protected $casts = [
		'widget_id' => 'int',
		'container_id' => 'int',
		'count' => 'int',
		'sort' => 'int'
	];

	protected $fillable = [
		'widget_id',
		'container_id',
		'title',
		'name',
		'type',
		'object',
		'value',
		'link',
		'lang',
		'count',
		'sort',
		'class',
		'widget_frontend',
		'widget_backend',
		'container_widget_backend',
		'update'
	];
}

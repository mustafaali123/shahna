<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class DeliveryTime
 * 
 * @property int $id
 * @property string $name
 * @property int $hours
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @package App\Models
 */
class DeliveryTime extends Model
{
	protected $table = 'delivery_time';

	protected $casts = [
		'hours' => 'int'
	];

	protected $fillable = [
		'name',
		'hours'
	];
}

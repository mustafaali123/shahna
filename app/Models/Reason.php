<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Reason
 * 
 * @property int $id
 * @property string $type
 * @property string $key
 * @property string $name
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @package App\Models
 */
class Reason extends Model
{
	protected $table = 'reasons';

	protected $fillable = [
		'type',
		'key',
		'name'
	];
}

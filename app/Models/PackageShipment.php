<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class PackageShipment
 * 
 * @property int $id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property int $package_id
 * @property int $shipment_id
 * @property string|null $description
 * @property float|null $weight
 * @property float|null $length
 * @property float|null $width
 * @property float|null $height
 * @property float|null $qty
 *
 * @package App\Models
 */
class PackageShipment extends Model
{
	protected $table = 'package_shipment';

	protected $casts = [
		'package_id' => 'int',
		'shipment_id' => 'int',
		'weight' => 'float',
		'length' => 'float',
		'width' => 'float',
		'height' => 'float',
		'qty' => 'float'
	];

	protected $fillable = [
		'package_id',
		'shipment_id',
		'description',
		'weight',
		'length',
		'width',
		'height',
		'qty'
	];
}

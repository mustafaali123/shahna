<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class LbContent
 * 
 * @property int $id
 * @property string|null $raw_content
 * @property string|null $rendered_content
 * @property string $contentable_type
 * @property int $contentable_id
 * @property string $type
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @package App\Models
 */
class LbContent extends Model
{
	protected $table = 'lb_contents';

	protected $casts = [
		'contentable_id' => 'int'
	];

	protected $fillable = [
		'raw_content',
		'rendered_content',
		'contentable_type',
		'contentable_id',
		'type'
	];
}

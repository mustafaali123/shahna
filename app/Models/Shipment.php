<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Shipment
 * 
 * @property int $id
 * @property string $code
 * @property int $status_id
 * @property int $type
 * @property int|null $branch_id
 * @property string $shipping_date
 * @property int|null $client_id
 * @property string|null $client_address
 * @property int|null $payment_type
 * @property int $paid
 * @property string $payment_integration_id
 * @property int|null $payment_method_id
 * @property float $tax
 * @property float $insurance
 * @property string|null $delivery_time
 * @property float $shipping_cost
 * @property float $total_weight
 * @property int|null $employee_user_id
 * @property string|null $client_street_address_map
 * @property string|null $client_lat
 * @property string|null $client_lng
 * @property string|null $client_url
 * @property string|null $reciver_street_address_map
 * @property string|null $reciver_lat
 * @property string|null $reciver_lng
 * @property string|null $reciver_url
 * @property string|null $attachments_before_shipping
 * @property string|null $attachments_after_shipping
 * @property string $client_phone
 * @property string $reciver_phone
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $reciver_name
 * @property string|null $reciver_address
 * @property int|null $mission_id
 * @property int|null $captain_id
 * @property float $return_cost
 * @property int $from_country_id
 * @property int $from_state_id
 * @property int|null $from_area_id
 * @property int $to_country_id
 * @property int $to_state_id
 * @property int|null $to_area_id
 * @property int|null $prev_branch
 * @property int $client_status
 * @property int $amount_to_be_collected
 * @property string|null $barcode
 *
 * @package App\Models
 */
class Shipment extends Model
{
	protected $table = 'shipments';

	protected $casts = [
		'status_id' => 'int',
		'type' => 'int',
		'branch_id' => 'int',
		'client_id' => 'int',
		'payment_type' => 'int',
		'paid' => 'int',
		'payment_method_id' => 'int',
		'tax' => 'float',
		'insurance' => 'float',
		'shipping_cost' => 'float',
		'total_weight' => 'float',
		'employee_user_id' => 'int',
		'mission_id' => 'int',
		'captain_id' => 'int',
		'return_cost' => 'float',
		'from_country_id' => 'int',
		'from_state_id' => 'int',
		'from_area_id' => 'int',
		'to_country_id' => 'int',
		'to_state_id' => 'int',
		'to_area_id' => 'int',
		'prev_branch' => 'int',
		'client_status' => 'int',
		'amount_to_be_collected' => 'int'
	];

	protected $fillable = [
		'code',
		'status_id',
		'type',
		'branch_id',
		'shipping_date',
		'client_id',
		'client_address',
		'payment_type',
		'paid',
		'payment_integration_id',
		'payment_method_id',
		'tax',
		'insurance',
		'delivery_time',
		'shipping_cost',
		'total_weight',
		'employee_user_id',
		'client_street_address_map',
		'client_lat',
		'client_lng',
		'client_url',
		'reciver_street_address_map',
		'reciver_lat',
		'reciver_lng',
		'reciver_url',
		'attachments_before_shipping',
		'attachments_after_shipping',
		'client_phone',
		'reciver_phone',
		'reciver_name',
		'reciver_address',
		'mission_id',
		'captain_id',
		'return_cost',
		'from_country_id',
		'from_state_id',
		'from_area_id',
		'to_country_id',
		'to_state_id',
		'to_area_id',
		'prev_branch',
		'client_status',
		'amount_to_be_collected',
		'barcode'
	];
}

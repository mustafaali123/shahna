<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Branch
 * 
 * @property int $id
 * @property int $code
 * @property int $type
 * @property string|null $img
 * @property string $name
 * @property string $email
 * @property string|null $responsible_name
 * @property string|null $responsible_mobile
 * @property string|null $national_id
 * @property int|null $government_id
 * @property string|null $area
 * @property string|null $address
 * @property int $is_archived
 * @property int $created_by
 * @property int $updated_by
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class Branch extends Model
{
	protected $table = 'branchs';

	protected $casts = [
		'code' => 'int',
		'type' => 'int',
		'government_id' => 'int',
		'is_archived' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int'
	];

	protected $fillable = [
		'code',
		'type',
		'img',
		'name',
		'email',
		'responsible_name',
		'responsible_mobile',
		'national_id',
		'government_id',
		'area',
		'address',
		'is_archived',
		'created_by',
		'updated_by'
	];
}

@extends('frontend.layouts.app')

@section('content')
   <section class="register">
      <div class="container">
        <!-- Start Header -->
         <header>
        <nav class="navbar navbar-expand-lg  fixed-top">
            <div class="container">
                <a class="navbar-brand" href="{{route('home')}}">
                    <img class="logo" src="{{ static_asset('assets/front/images/logo.png')}}" alt="">
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="{{route('home')}}">الصفحة الرئيسية</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('requestshipping')}}">طلب شحنة</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{route('followshipping')}}">تتبع شحنتك</a>
                        </li>
                    </ul>
                  
                        <div class="registeration">
                          <button class="btn-auth"><a class="register d-block " href="{{route('user.registration')}}">Register</a></button>
                        </div>
                </div>
            </div>
        </nav>
    </header>
        <!-- End Header -->

        <!-- Start Sign  Form -->
        <form action="" class="sign-form forget-password">
          <h3 class="title text-center">نسيت كلمة مرورك ؟</h3>
          <p class="text text-center">ادخل بريدك الالكتروني حتي نرسل رابط اعادة تعيين كلمة المرور له</p>
          <div class="form-group">
            <div class="details">
              <label class="form-label" for="">البريد الالكتروني</label>
              <div class="input-container">
                <input class="form-input different" type="email">
                <i class="far fa-envelope icon"></i>
              </div>
            </div>
          </div>
          <button class="submit-button"><a class="link d-block" href="#">ارسال</a></button>
        </form>
        <!-- End Sign  Form -->
      </div>
    </section>
@endsection

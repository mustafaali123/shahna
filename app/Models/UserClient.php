<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UserClient
 * 
 * @property int $id
 * @property int $user_id
 * @property int $client_id
 *
 * @package App\Models
 */
class UserClient extends Model
{
	protected $table = 'user_client';
	public $timestamps = false;

	protected $casts = [
		'user_id' => 'int',
		'client_id' => 'int'
	];

	protected $fillable = [
		'user_id',
		'client_id'
	];
}

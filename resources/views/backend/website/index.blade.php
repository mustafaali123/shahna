@extends('backend.layouts.app')

@section('subheader')
    <!--begin::Subheader-->
    <div class="subheader py-2 py-lg-6 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <!--begin::Info-->
            <div class="d-flex align-items-center flex-wrap mr-1">
                <!--begin::Page Heading-->
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <!--begin::Page Title-->
                    <h5 class="text-dark font-weight-bold my-1 mr-5">{{ translate('Menus') }}</h5>
                    <!--end::Page Title-->
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item text-muted">
                            <a href="{{ route('admin.dashboard')}}" class="text-muted">{{translate('Dashboard')}}</a>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="#" class="text-muted">{{ translate('Menus') }}</a>
                        </li>
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page Heading-->
            </div>
            <!--end::Info-->
        </div>
    </div>
    <!--end::Subheader-->
@endsection

@section('content')
    <div class="card">
        <div class="card-body">
            <form class="form-horizontal" action="{{ route('admin.websites.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="card-body">
                     @foreach ($data as $row)
                     @if ($row->type=='string')
                    <div class="form-group row">
                        <label class="col-sm-3 col-from-label" for="name">{{translate(str_replace('_',' ',$row->option))}}</label>
                        <div class="col-sm-9">
                            <input type="text" placeholder="{{translate(str_replace('_',' ',$row->option))}}" id="{{$row->option}}" name="{{$row->option}}" value="{{ old($row->value , $row->value  ) }}" class="form-control" required>
                        </div>
                    </div>

                     @elseif ($row->type=='text')
                    <div class="form-group row">
                        <label class="col-sm-3 col-from-label" for="name">{{translate(str_replace('_',' ',$row->option))}}</label>
                        <div class="col-sm-9">
                            <textarea type="text" placeholder="{{translate(str_replace('_',' ',$row->option))}}" id="{{$row->option}}" name="{{$row->option}}"  class="form-control" >{{ old($row->value , $row->value  ) }}</textarea>
                        </div>
                    </div>
                     @elseif ($row->type=='image')
                    <div class="form-group row">
                        <label class="col-sm-3 col-from-label" for="name">{{translate(str_replace('_',' ',$row->option))}}</label>
                        <div class="col-sm-6">
                            <input type="file" placeholder="{{translate(str_replace('_',' ',$row->option))}}" id="{{$row->option}}" name="{{$row->option}}"  class="form-control" >
                        </div>
                        <div class="col-sm-3">
                            @if($row->value)
                            <img src="{{static_asset('websites/'.$row->value)}}" width="120px">
                            @endif
                        </div>
                    </div>
                    @endif
                    @endforeach


                    <div class="form-group mb-0 text-right">
                        <button type="submit" class="btn btn-sm btn-primary">{{translate('Save')}}</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection

@section('script')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>


@endsection

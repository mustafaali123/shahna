<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class PageTranslation
 * 
 * @property int $id
 * @property int $page_id
 * @property string $title
 * @property string $content
 * @property string $lang
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class PageTranslation extends Model
{
	protected $table = 'page_translations';

	protected $casts = [
		'page_id' => 'int'
	];

	protected $fillable = [
		'page_id',
		'title',
		'content',
		'lang'
	];
}

<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Payment
 * 
 * @property int $id
 * @property int $seller_id
 * @property int|null $shipment_id
 * @property float $amount
 * @property string|null $payment_details
 * @property string|null $payment_method
 * @property string|null $txn_code
 * @property Carbon|null $payment_date
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class Payment extends Model
{
	protected $table = 'payments';

	protected $casts = [
		'seller_id' => 'int',
		'shipment_id' => 'int',
		'amount' => 'float'
	];

	protected $dates = [
		'payment_date'
	];

	protected $fillable = [
		'seller_id',
		'shipment_id',
		'amount',
		'payment_details',
		'payment_method',
		'txn_code',
		'payment_date'
	];
}

<?php
/*
|--------------------------------------------------------------------------
| Update Routes
|--------------------------------------------------------------------------
|
| This route is responsible for handling the intallation process
|
|
|
*/
use Illuminate\Support\Facades\Route;

Route::get('/', 'UpdateController@step0');

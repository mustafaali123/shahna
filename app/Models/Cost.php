<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Cost
 * 
 * @property int $id
 * @property int $from_country_id
 * @property int $from_state_id
 * @property int|null $from_area_id
 * @property int $to_country_id
 * @property int $to_state_id
 * @property int|null $to_area_id
 * @property float $shipping_cost
 * @property float $return_cost
 * @property float $tax
 * @property float $insurance
 * @property float $mile_cost
 * @property float $return_mile_cost
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @package App\Models
 */
class Cost extends Model
{
	protected $table = 'costs';

	protected $casts = [
		'from_country_id' => 'int',
		'from_state_id' => 'int',
		'from_area_id' => 'int',
		'to_country_id' => 'int',
		'to_state_id' => 'int',
		'to_area_id' => 'int',
		'shipping_cost' => 'float',
		'return_cost' => 'float',
		'tax' => 'float',
		'insurance' => 'float',
		'mile_cost' => 'float',
		'return_mile_cost' => 'float'
	];

	protected $fillable = [
		'from_country_id',
		'from_state_id',
		'from_area_id',
		'to_country_id',
		'to_state_id',
		'to_area_id',
		'shipping_cost',
		'return_cost',
		'tax',
		'insurance',
		'mile_cost',
		'return_mile_cost'
	];
}

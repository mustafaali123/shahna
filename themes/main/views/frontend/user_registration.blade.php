@extends('frontend.layouts.request')

@section('style')
<link rel="stylesheet" href="{{ static_asset('assets/front/css/pages/signup.css')}}" />

@endsection
@section('content')
    <section class="register m-top-100">
      <div class="container">
     



        <form  class="sign-form" action="{{ route('client.save') }}" method="POST">
            @csrf
          <h3 class="title text-center">انشاء حساب</h3>
          <div class="form-group "><!-- Start Full name -->
            <div class="details">
              <label class="form-label" for=""> {{  translate('Full Name') }}</label>
              <div class="input-container">
                <input type="text" class="form-input {{ $errors->has('name') ? ' is-invalid' : '' }}" value="{{ old('name') }}" placeholder="{{  translate('Full Name') }}" name="name">
                <i class="fas fa-user icon"></i>
                @if ($errors->has('name'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
                 @endif
              </div>
            </div>
          </div><!-- End Full name -->
          <div class="form-group"><!-- Start Email -->
            <div class="details">
              <label class="form-label" for="">{{  translate('Email') }}</label>
              <div class="input-container">
                <input type="text" class="form-input different {{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{ old('email') }}" placeholder="{{  translate('Email') }}" name="email">
                <i class="far fa-envelope icon"></i>
                @if ($errors->has('email'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif
              </div>
            </div>
          </div><!-- End Email -->
          <div class="form-group"><!-- Start Email -->
            <div class="details">
              <label class="form-label" for="">{{  translate('Mobile') }}</label>
              <div class="input-container">
                <input type="text" class="form-input different {{ $errors->has('mobile') ? ' is-invalid' : '' }}" value="{{ old('mobile') }}" placeholder="{{  translate('mobile') }}" name="mobile">
                <i class="far fa-envelope icon"></i>
                @if ($errors->has('mobile'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('mobile') }}</strong>
                </span>
            @endif
              </div>
            </div>
          </div><!-- End mobile -->
          <div class="form-group"><!-- Start password -->
            <div class="details">
              <label class="form-label" for=""> {{  translate('Password') }}</label>
              <div class="input-container">
                <input type="password" class="form-input different {{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="{{  translate('Password') }}" name="password">
                <i class="fas fa-lock icon"></i>
                @if ($errors->has('password'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
            @endif
              </div>
            </div>
          </div><!-- End password -->
          <div class="form-group"><!-- Start password -->
            <div class="details">
              <label class="form-label" for=""> {{  translate('Confirm Password') }}</label>
              <div class="input-container">
                <input type="password" class="form-input different {{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="{{  translate('Confirm Password') }}" name="password_confirmation">
                <i class="fas fa-lock icon"></i>
                @if ($errors->has('password_confirmation'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                </span>
            @endif
              </div>
            </div>
          </div><!-- End password -->
          <div class="form-group"><!-- Start password -->
            <div class="detais">
              <label class="form-label" for=""></label>
              <div class="input-continer">
                <input type="checkbox" name="condotion_agreement" required>
                <span class=opacity-60>{{ translate('By signing up you agree to our terms and conditions.')}}</span>
                <span class="aiz-square-check"></span>
                @if ($errors->has('condotion_agreement'))
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $errors->first('condotion_agreement') }}</strong>
                </span>
            @endif
                <i class="fas fa-lock icon"></i>
              </div>
            </div>
          </div><!-- End password -->
          <button class="submit-button" type="submit">انشاء حساب</button>
        </form>
        <!-- End Sign Form -->

        <div class="text-center account">
          <span>هل لديك حساب الفعل ؟</span>
          <span ><a class="go-to-sign" href="{{route('user.login')}}">سجل دخولك</a></span>
        </div>

      </div>
    </section>
@endsection

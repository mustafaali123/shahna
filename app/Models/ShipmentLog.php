<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ShipmentLog
 * 
 * @property int $id
 * @property int $from
 * @property int $to
 * @property int $created_by
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property int $shipment_id
 *
 * @package App\Models
 */
class ShipmentLog extends Model
{
	protected $table = 'shipment_log';

	protected $casts = [
		'from' => 'int',
		'to' => 'int',
		'created_by' => 'int',
		'shipment_id' => 'int'
	];

	protected $fillable = [
		'from',
		'to',
		'created_by',
		'shipment_id'
	];
}

<!DOCTYPE html>
<html lang="ar" dir="rtl"> 
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="libs/bootstrap/css/bootstrap.rtl.min.css" />
    <link rel="stylesheet" href="libs/fontawesome/css/all.css" />
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,400;0,700;1,500&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="css/pages/signup.css" />
    <title>Sign in</title>
  </head>
  <body>
    <section class="register">
      <div class="container">
        <!-- Start Header -->
        <header class="registration-header d-flex justify-content-between align-items-center">
          <div class="languages">
            <span class="active">عربي</span>
            <span>English</span>
          </div>
          <div class="logo-container">
            <a href="index.html"><img class="logo"  src="./images/logo.png" alt="logo"></a>
          </div>
        </header>
        <!-- End Header -->

        <!-- Start Sign  Form -->
        <form action="" class="sign-form sign-in">
          <h3 class="title text-center">تسجيل الدخول</h3>
          <div class="form-group"><!-- Start Email -->
            <div class="details">
              <label class="form-label" for="">البريد الالكتروني</label>
              <div class="input-container">
                <input class="form-input different" type="email">
                <i class="far fa-envelope icon"></i>
              </div>
            </div>
          </div><!-- End Email -->
          <div class="form-group"><!-- Start password -->
            <div class="details">
              <label class="form-label" for="">كلمة المرور</label>
              <div class="input-container">
                <input class="form-input different" type="password">
                <i class="fas fa-lock icon"></i>
              </div>
            </div>
          </div><!-- End password -->
          <button class="submit-button">تسجيل الدخول</button>
        </form>
        <!-- End Sign  Form -->

        <div class="forget-password text-center d-flex justify-content-around">
          <div class="remember-me">
            <input type="checkbox">
            <span>تذكرنى</span>
          </div>
          <span>
            <a class="newpassword" href="forgetpassword.html">هل نسيت كلمة المرور ؟</a>
          </span>
        </div>

        <div class="text-center account">
          <span>ليس لديك حساب ؟</span>
          <span ><a class="go-to-sign" href="signup.html">انشئ حساب</a></span>
        </div>

      </div>
    </section>
  </body>
</html>

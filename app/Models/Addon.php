<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Addon
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $unique_identifier
 * @property string|null $version
 * @property int $activated
 * @property string|null $image
 * @property Carbon|null $created_at
 * @property Carbon $updated_at
 * @property string|null $files
 * @property string|null $required_addons
 *
 * @package App\Models
 */
class Addon extends Model
{
	protected $table = 'addons';

	protected $casts = [
		'activated' => 'int'
	];

	protected $fillable = [
		'name',
		'unique_identifier',
		'version',
		'activated',
		'image',
		'files',
		'required_addons'
	];
}

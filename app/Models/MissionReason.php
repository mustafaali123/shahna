<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class MissionReason
 * 
 * @property int $id
 * @property int|null $mission_id
 * @property int|null $reason_id
 * @property string|null $type
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @package App\Models
 */
class MissionReason extends Model
{
	protected $table = 'mission_reasons';

	protected $casts = [
		'mission_id' => 'int',
		'reason_id' => 'int'
	];

	protected $fillable = [
		'mission_id',
		'reason_id',
		'type'
	];
}

<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class AdminMenu
 * 
 * @property int $id
 * @property string $name
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * 
 * @property Collection|AdminMenuItem[] $admin_menu_items
 *
 * @package App\Models
 */
class AdminMenu extends Model
{
	protected $table = 'admin_menus';

	protected $fillable = [
		'name'
	];

	public function admin_menu_items()
	{
		return $this->hasMany(AdminMenuItem::class, 'menu');
	}
}

<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UserCaptain
 * 
 * @property int $id
 * @property int $user_id
 * @property int $captain_id
 *
 * @package App\Models
 */
class UserCaptain extends Model
{
	protected $table = 'user_captain';
	public $timestamps = false;

	protected $casts = [
		'user_id' => 'int',
		'captain_id' => 'int'
	];

	protected $fillable = [
		'user_id',
		'captain_id'
	];
}

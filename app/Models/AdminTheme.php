<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class AdminTheme
 * 
 * @property int $id
 * @property string $name
 * @property string|null $title
 * @property string|null $img
 * @property string|null $description
 * @property bool $active
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @package App\Models
 */
class AdminTheme extends Model
{
	protected $table = 'admin_themes';

	protected $casts = [
		'active' => 'bool'
	];

	protected $fillable = [
		'name',
		'title',
		'img',
		'description',
		'active'
	];
}

<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Language
 * 
 * @property int $id
 * @property string $name
 * @property string $code
 * @property string|null $icon
 * @property int $rtl
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class Language extends Model
{
	protected $table = 'languages';

	protected $casts = [
		'rtl' => 'int'
	];

	protected $fillable = [
		'name',
		'code',
		'icon',
		'rtl'
	];
}

<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class AdminContainer
 * 
 * @property int $id
 * @property string|null $title
 * @property string $name
 * @property string $theme_name
 * @property string $type
 * @property bool $active
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @package App\Models
 */
class AdminContainer extends Model
{
	protected $table = 'admin_containers';

	protected $casts = [
		'active' => 'bool'
	];

	protected $fillable = [
		'title',
		'name',
		'theme_name',
		'type',
		'active'
	];
}

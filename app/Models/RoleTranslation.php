<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class RoleTranslation
 * 
 * @property int $id
 * @property int $role_id
 * @property string $name
 * @property string $lang
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class RoleTranslation extends Model
{
	protected $table = 'role_translations';

	protected $casts = [
		'role_id' => 'int'
	];

	protected $fillable = [
		'role_id',
		'name',
		'lang'
	];
}

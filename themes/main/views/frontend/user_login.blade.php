@extends('frontend.layouts.request')

@section('style')

<link rel="stylesheet" href="{{ static_asset('assets/front/css/pages/signup.css')}}" />

@endsection

@section('content')
     <section class="register m-top-100">
      <div class="container">

      
        <form  class="sign-form sign-in" action="{{ route('login') }}" method="POST">
            @csrf
          <h3 class="title text-center"> {{  translate('Login') }}</h3>
          <div class="form-group"><!-- Start Email -->
            <div class="details">
              <label class="form-label" for="">{{ translate('Email Or Phone')}} </label>
              <div class="input-container">

                @if (\App\Addon::where('unique_identifier', 'otp_system')->first() != null && \App\Addon::where('unique_identifier', 'otp_system')->first()->activated)
                    <input type="text" class="form-input different {{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{ old('email') }}" placeholder="{{ translate('Email Or Phone')}}" name="email" id="email">
                @else
                    <input type="text" class="form-input different {{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{ old('email') }}" placeholder="{{  translate('Email') }}" name="email">
                @endif
                <i class="far fa-envelope icon"></i>
                @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
              </div>
            </div>
          </div><!-- End Email -->
          <div class="form-group"><!-- Start password -->
            <div class="details">
              <label class="form-label" for="">كلمة المرور</label>
              <div class="input-container">
                <input type="password" class="form-input different {{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="{{ translate('Password')}}" name="password" id="password">
                <i class="fas fa-lock icon"></i>
                @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
              </div>
            </div>
          </div><!-- End password -->
          <button class="submit-button" type="submit"> {{  translate('Login') }}</button>
        </form>
        <!-- End Sign  Form -->

        <div class="forget-password text-center d-flex justify-content-around">
          <div class="remember-me">
            <input type="checkbox">
            <span>تذكرنى</span>
          </div>
          <span>
            <a class="newpassword" href="{{route('forget.password')}}">هل نسيت كلمة المرور ؟</a>
          </span>
        </div>

        <div class="text-center account">
          <span>ليس لديك حساب ؟</span>
          <span ><a class="go-to-sign" href="{{route('user.registration')}}">انشئ حساب</a></span>
        </div>

      </div>
    </section>
@endsection

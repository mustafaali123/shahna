<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class GeneralSetting
 * 
 * @property int $id
 * @property string $frontend_color
 * @property string|null $logo
 * @property string|null $footer_logo
 * @property string|null $admin_logo
 * @property string|null $admin_login_background
 * @property string|null $admin_login_sidebar
 * @property string|null $favicon
 * @property string|null $site_name
 * @property string|null $address
 * @property string $description
 * @property string|null $phone
 * @property string|null $email
 * @property string|null $facebook
 * @property string|null $instagram
 * @property string|null $twitter
 * @property string|null $youtube
 * @property string|null $google_plus
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class GeneralSetting extends Model
{
	protected $table = 'general_settings';

	protected $fillable = [
		'frontend_color',
		'logo',
		'footer_logo',
		'admin_logo',
		'admin_login_background',
		'admin_login_sidebar',
		'favicon',
		'site_name',
		'address',
		'description',
		'phone',
		'email',
		'facebook',
		'instagram',
		'twitter',
		'youtube',
		'google_plus'
	];
}

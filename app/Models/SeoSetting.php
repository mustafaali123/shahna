<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class SeoSetting
 * 
 * @property int $id
 * @property string $keyword
 * @property string $author
 * @property int $revisit
 * @property string $sitemap_link
 * @property string $description
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class SeoSetting extends Model
{
	protected $table = 'seo_settings';

	protected $casts = [
		'revisit' => 'int'
	];

	protected $fillable = [
		'keyword',
		'author',
		'revisit',
		'sitemap_link',
		'description'
	];
}

@extends('frontend.layouts.request')

@section('content')


    <!-- Start Description   -->
    <section class="descrtion" style="background-image:url('{{static_asset('websites/'.website_info('background'))}}');height: 50%; background-position: center; background-repeat: no-repeat ;background-size: cover;">
        <div class="container">
            <div class="shipment">
                <div class="row">
                    <div class="details col-md-8">
                        {{-- <h2 class="title">{{ website_info('home_title_'.app()->getLocale()) }}</h2>

                        <p class="text">{{ website_info('home_text_'.app()->getLocale()) }}</p> --}}
                        <div class="follow-shipment">
                            <form class="form" action="{{route('shipmentsclient.tracking')}}" method="GET">
                                <button class="shipment-btn" type="submit">تعقب شحنتك</button>
                                <input class="shipment-input" type="text" name="code" placeholder="{{translate('Example: SH00001')}}">
                            </form>
                        </div>
                    </div>
                    <div class="photo col-md-4">
                        {{-- <img class="w-100" src="{{ static_asset('assets/front/images/home/01.png')}}" alt=""> --}}
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!-- End Description   -->

    <!-- Strat Our Services -->
    <section class="our-services">
        <div class="container">
            <h2 class="main-title text-center">{{ translate('Our Services') }}</h2>
            <!-- Start Service -->
            <div class="box-shipment" >
                <div class="row">
                    <div class="details col-md-8">
                        <h3 class="title">{{ website_info('service_title1_'.app()->getLocale()) }} </h3>
                        <p class="text">{{ website_info('service_text1_'.app()->getLocale()) }}</p>
                    </div>
                    <div class="photo col-md-4">
                        <img class="w-100" src="{{static_asset('websites/'.website_info('service_image1'))}}" alt="">
                    </div>
                </div>
            </div>
            <!-- End Service -->
            <!-- Start Shipment -->
            <div class="following-shipment">
                <div class="row">
                    <div class="photo col-md-4">
                        <img class="w-100" src="{{static_asset('websites/'.website_info('service_image2'))}}" alt="">
                    </div>
                    <div class="details col-md-8">
                        <h3 class="title">{{ website_info('service_title2_'.app()->getLocale()) }} </h3>
                        <p >{{ website_info('service_text2_'.app()->getLocale()) }}</p>
                    </div>
                </div>
            </div>
            <!-- End Shipment -->
        </div>
    </section>
@endsection


<!DOCTYPE html>
@if(\App\Language::where('code', Session::get('locale', Config::get('app.locale')))->first()->rtl == 1)
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}" direction="rtl" dir="rtl" style="direction: rtl;">
@else
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@endif


	<meta name="csrf-token" content="{{ csrf_token() }}">
	<meta name="app-url" content="{{ getBaseURL() }}">
	<meta name="file-base-url" content="{{ getFileBaseURL() }}">
	<base href="">
	<meta charset="utf-8" />
	<link rel="icon" href="@if(get_setting('site_icon')) {{uploaded_asset(get_setting('site_icon'))}} @else {{static_asset('assets/dashboard/media/logos/favicon.ico')}} @endif">
	@if(get_setting('site_name'))
		<title> @if(View::hasSection('sub_title')) @yield('sub_title') | @endif {{ get_setting('site_name') }}</title>
	@else
		<title>@if(View::hasSection('sub_title')) @yield('sub_title') | @endif {{ translate('SofGates Framework') }} </title>
	@endif
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

	<!--begin::Fonts-->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />
	<!--end::Fonts-->

	@if(\App\Language::where('code', Session::get('locale', Config::get('app.locale')))->first()->rtl == 1)

	<link href="https://fonts.googleapis.com/css2?family=Cairo" rel="stylesheet">
	<!--begin::Page Vendors Styles(used by this page)-->
	<link href="{{ static_asset('assets/dashboard/plugins/custom/fullcalendar/fullcalendar.bundle.rtl.css') }}"
		rel="stylesheet" type="text/css" />

	<!--end::Page Vendors Styles-->

	<!--begin::Global Theme Styles(used by all pages)-->
	<link href="{{ static_asset('assets/dashboard/plugins/global/plugins.bundle.rtl.css') }}" rel="stylesheet"
		type="text/css" />
	<link href="{{ static_asset('assets/dashboard/plugins/custom/prismjs/prismjs.bundle.rtl.css') }}" rel="stylesheet"
		type="text/css" />
	<link href="{{ static_asset('assets/dashboard/css/style.bundle.rtl.css') }}" rel="stylesheet" type="text/css" />
	<!--end::Global Theme Styles-->

	<!--begin::Layout Themes(used by all pages)-->
	<link href="{{ static_asset('assets/dashboard/css/themes/layout/header/base/light.rtl.css') }}" rel="stylesheet"
		type="text/css" />
	<link href="{{ static_asset('assets/dashboard/css/themes/layout/header/menu/light.rtl.css') }}" rel="stylesheet"
		type="text/css" />
	<link href="{{ static_asset('assets/dashboard/css/themes/layout/brand/light.rtl.css') }}" rel="stylesheet"
		type="text/css" />
	<link href="{{ static_asset('assets/dashboard/css/themes/layout/aside/light.rtl.css') }}" rel="stylesheet"
		type="text/css" />
	<!--end::Layout Themes-->
	@else
	<!--begin::Page Vendors Styles(used by this page)-->
	<link href="{{ static_asset('assets/dashboard/plugins/custom/fullcalendar/fullcalendar.bundle.css') }}"
		rel="stylesheet" type="text/css" />

	<!--end::Page Vendors Styles-->

	<!--begin::Global Theme Styles(used by all pages)-->
	<link href="{{ static_asset('assets/dashboard/plugins/global/plugins.bundle.css') }}" rel="stylesheet"
		type="text/css" />
	<link href="{{ static_asset('assets/dashboard/plugins/custom/prismjs/prismjs.bundle.css') }}" rel="stylesheet"
		type="text/css" />
	<link href="{{ static_asset('assets/dashboard/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
	<!--end::Global Theme Styles-->

	<!--begin::Layout Themes(used by all pages)-->
	<link href="{{ static_asset('assets/dashboard/css/themes/layout/header/base/light.css') }}" rel="stylesheet"
		type="text/css" />
	<link href="{{ static_asset('assets/dashboard/css/themes/layout/header/menu/light.css') }}" rel="stylesheet"
		type="text/css" />
	<link href="{{ static_asset('assets/dashboard/css/themes/layout/brand/light.css') }}" rel="stylesheet"
		type="text/css" />
	<link href="{{ static_asset('assets/dashboard/css/themes/layout/aside/light.css') }}" rel="stylesheet"
		type="text/css" />
	<!--end::Layout Themes-->
	@endif
	<link href="{{ static_asset('assets/css/custom-style.css?v=7.2.3') }}" rel="stylesheet" type="text/css" />

    <link href="{{ static_asset('assets/dashboard/plugins/global/plugins.bundle.rtl.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{ static_asset('assets/front/libs/bootstrap/css/bootstrap.rtl.min.css') }}" />
    <link rel="stylesheet" href="{{ static_asset('assets/front/libs/fontawesome/css/all.css')}}" />
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,400;0,700;1,500&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{ static_asset('assets/front/css/pages/main.css')}}" />
    <link rel="stylesheet" href="{{ static_asset('assets/front/css/pages/followingshipment.css')}}" />

    <link rel="stylesheet" href="{{ static_asset('assets/front/css/pages/shanaform.css')}}" />
	@yield('style')
    <style>
        .m-top-100{
            margin-top:13%
        }
    </style>
	<script>
		var AIZ = AIZ || {};
	</script>



    <title>Shahna</title>
</head>

<!-- End Header  -->
<body id="kt_body" class="header-fixed header-mobile-fixed aside-enabled aside-fixed aside-minimize-hoverable page-loading">
    <div class="d-flex flex-column flex-root" >
        <div class="flex-row d-flex flex-column-fluid page">
            <div class="d-flex flex-column flex-row-fluid" id="kt_wrapper">
                <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
                    <div class="d-flex flex-column-fluid">
                        <div class="container-fluid">
                            <header>

                                <nav class="navbar navbar-expand-lg  fixed-top">
                                    <div class="container">

                                        <a class="navbar-brand" href="{{route('home')}}">
                                            <img class="logo" src="{{ static_asset('assets/front/images/logo.png')}}" alt="">
                                        </a>
                                        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                    <span class="navbar-toggler-icon"></span>
                                  </button>
                                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                                                <li class="nav-item">
                                                    <a class="nav-link {{ Route::currentRouteName()=='home'?'active':''}}" aria-current="page" href="{{route('home')}}">{{ translate('Home') }}  </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link {{ Route::currentRouteName()=='requestshipping'?'active':''}}" href="{{route('requestshipping')}}">{{ translate('Request Shipping') }} </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link {{ Route::currentRouteName()=='followshipping'?'active':''}}" href="{{route('followshipping')}}"> {{ translate('Follow Shipping') }} </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link {{ Route::currentRouteName()=='clientrequests'?'active':''}}" href="{{route('clientrequests')}}"> {{ translate('Requests Date') }} </a>
                                                </li>

                                            </ul>
                                            <div class="registeration">
                                                @if(Auth::check())
                                                    <button class="btn-auth"><a class="register d-block " href="{{ route('logout') }}">{{ translate('Logout') }}</a></button>
                                                @else
                                                 <button class="btn-auth"><a class="register d-block " href="{{route('user.registration')}}">{{ translate('Register') }}</a></button>
                                                @endif
                                                @if(app()->getLocale()=='ar')
                                                    <a class="btn  btn-warning" href="{{route('select','en')}}">English</a>
                                                @else
                                                    <a class="btn btn-warning " href="{{route('select','ar')}}">Arabic</a>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </nav>
                            </header>
                            @yield('content')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <script src="{{ static_asset('assets/front/libs/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
        <script src="{{ static_asset('assets/dashboard/plugins/global/plugins.bundle.js') }}"></script>
        <script src="{{ static_asset('assets/dashboard/plugins/custom/prismjs/prismjs.bundle.js') }}"></script>
        <script src="{{ static_asset('assets/dashboard/js/scripts.bundle.js') }}"></script>
        <!--end::Global Theme Bundle-->

        <!--begin::Page Vendors(used by this page)-->
        <script src="{{ static_asset('assets/dashboard/plugins/custom/fullcalendar/fullcalendar.bundle.js') }}"></script>
        <!--end::Page Vendors-->



        <script src="{{ static_asset('assets/js/vendors.js') }}" ></script>
        <script src="{{ static_asset('assets/js/aiz-core.js') }}" ></script>

        @yield('script')

        <script type="text/javascript">
            @foreach (session('flash_notification', collect())->toArray() as $message)
                AIZ.plugins.notify('{{ $message['level'] }}', '{{ $message['message'] }}');
                @php
                session()->forget('flash_notification')
                @endphp
            @endforeach

            @if (count($errors) > 0)
                @foreach ($errors->all() as $error)
                    AIZ.plugins.notify('warning', '{{ $error }}');
                @endforeach
            @endif

            @if ($msg = Session::get('status'))
                AIZ.plugins.notify('success', '{{ $msg }}');
            @endif

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            if ($('#lang-change').length > 0) {
                $('#lang-change .navi-item a').each(function() {
                    $(this).on('click', function(e){
                        e.preventDefault();
                        var $this = $(this);
                        var locale = $this.data('flag');
                        $.post('{{ route('language.change') }}',{_token:'{{ csrf_token() }}', locale:locale}, function(data){
                            location.reload();
                        });
                    });
                });
            }
        </script>

        <!--begin::Page Scripts(used by this page)-->
        <script src="{{ static_asset('assets/dashboard/js/pages/widgets.js') }}"></script>
        <!--end::Page Scripts-->
    </body>

    <!--end::Body-->

    </html>


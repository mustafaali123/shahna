<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class ShipmentMission
 * 
 * @property int $id
 * @property int $shipment_id
 * @property int $mission_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @package App\Models
 */
class ShipmentMission extends Model
{
	protected $table = 'shipment_mission';

	protected $casts = [
		'shipment_id' => 'int',
		'mission_id' => 'int'
	];

	protected $fillable = [
		'shipment_id',
		'mission_id'
	];
}

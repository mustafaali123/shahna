<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Search
 * 
 * @property int $id
 * @property string $query
 * @property int $count
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class Search extends Model
{
	protected $table = 'searches';

	protected $casts = [
		'count' => 'int'
	];

	protected $fillable = [
		'query',
		'count'
	];
}

<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Package
 * 
 * @property int $id
 * @property string $name
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property bool $cost
 *
 * @package App\Models
 */
class Package extends Model
{
	protected $table = 'packages';

	protected $casts = [
		'cost' => 'bool'
	];

	protected $fillable = [
		'name',
		'cost'
	];
}

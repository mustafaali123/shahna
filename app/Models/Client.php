<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Client
 * 
 * @property int $id
 * @property int $code
 * @property int $type
 * @property string|null $img
 * @property string $name
 * @property string $email
 * @property string|null $responsible_name
 * @property string|null $responsible_mobile
 * @property string|null $follow_up_name
 * @property string|null $follow_up_mobile
 * @property string|null $how_know_us
 * @property string|null $national_id
 * @property int|null $government_id
 * @property string|null $area
 * @property string|null $address
 * @property int $is_archived
 * @property string|null $created_by_type
 * @property int $created_by
 * @property int $updated_by
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property float $pickup_cost
 * @property float $supply_cost
 * @property float $def_mile_cost
 * @property float $def_shipping_cost
 * @property float $def_tax
 * @property float $def_insurance
 * @property float $def_return_mile_cost
 * @property float $def_return_cost
 * @property float $def_shipping_cost_gram
 * @property float $def_mile_cost_gram
 * @property float $def_tax_gram
 * @property float $def_insurance_gram
 * @property float $def_return_cost_gram
 * @property float $def_return_mile_cost_gram
 *
 * @package App\Models
 */
class Client extends Model
{
	protected $table = 'clients';

	protected $casts = [
		'code' => 'int',
		'type' => 'int',
		'government_id' => 'int',
		'is_archived' => 'int',
		'created_by' => 'int',
		'updated_by' => 'int',
		'pickup_cost' => 'float',
		'supply_cost' => 'float',
		'def_mile_cost' => 'float',
		'def_shipping_cost' => 'float',
		'def_tax' => 'float',
		'def_insurance' => 'float',
		'def_return_mile_cost' => 'float',
		'def_return_cost' => 'float',
		'def_shipping_cost_gram' => 'float',
		'def_mile_cost_gram' => 'float',
		'def_tax_gram' => 'float',
		'def_insurance_gram' => 'float',
		'def_return_cost_gram' => 'float',
		'def_return_mile_cost_gram' => 'float'
	];

	protected $fillable = [
		'code',
		'type',
		'img',
		'name',
		'email',
		'responsible_name',
		'responsible_mobile',
		'follow_up_name',
		'follow_up_mobile',
		'how_know_us',
		'national_id',
		'government_id',
		'area',
		'address',
		'is_archived',
		'created_by_type',
		'created_by',
		'updated_by',
		'pickup_cost',
		'supply_cost',
		'def_mile_cost',
		'def_shipping_cost',
		'def_tax',
		'def_insurance',
		'def_return_mile_cost',
		'def_return_cost',
		'def_shipping_cost_gram',
		'def_mile_cost_gram',
		'def_tax_gram',
		'def_insurance_gram',
		'def_return_cost_gram',
		'def_return_mile_cost_gram'
	];
}

<!DOCTYPE html>
<html lang="ar" dir="rtl"> 
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="libs/bootstrap/css/bootstrap.rtl.min.css" />
    <link rel="stylesheet" href="libs/fontawesome/css/all.css" />
    <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,400;0,700;1,500&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="css/pages/signup.css" />
    <title>Success Change Password</title>
  </head>
  <body>
    <section class="register">
      <div class="container">
        <!-- Start Header -->
        <header class="registration-header d-flex justify-content-center align-items-center">
          <div class="logo-container">
            <a href="index.html"><img class="logo"  src="./images/logo.png" alt="logo"></a>
          </div>
        </header>
        <!-- End Header -->

        <form action="" class="sign-form success-password">
          <h3 class="title text-center">تم تغيير كلمة المرور بنجاح</h3>
          <p class="text text-center">سوف يتم تحويلك لصفحة تسجيل الدخول حتي نتأكد معا من كلمة مرورك الجديدة</p>
          <button class="submit-button"><a class="link d-block" href="signin.html">تسجيل الدخول</a></button>
        </form>
      </div>
    </section>
  </body>
</html>

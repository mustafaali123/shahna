<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Currency
 * 
 * @property int $id
 * @property string $name
 * @property string $symbol
 * @property float $exchange_rate
 * @property int $status
 * @property string|null $code
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class Currency extends Model
{
	protected $table = 'currencies';
	public $incrementing = false;

	protected $casts = [
		'id' => 'int',
		'exchange_rate' => 'float',
		'status' => 'int'
	];

	protected $fillable = [
		'id',
		'name',
		'symbol',
		'exchange_rate',
		'status',
		'code'
	];
}

<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class AddressClient
 * 
 * @property int $id
 * @property int $client_id
 * @property string|null $address
 * @property int $country_id
 * @property int $state_id
 * @property int|null $area_id
 * @property string|null $client_street_address_map
 * @property string|null $client_lat
 * @property string|null $client_lng
 * @property string|null $client_url
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 *
 * @package App\Models
 */
class AddressClient extends Model
{
	protected $table = 'address_client';

	protected $casts = [
		'client_id' => 'int',
		'country_id' => 'int',
		'state_id' => 'int',
		'area_id' => 'int'
	];

	protected $fillable = [
		'client_id',
		'address',
		'country_id',
		'state_id',
		'area_id',
		'client_street_address_map',
		'client_lat',
		'client_lng',
		'client_url'
	];
}

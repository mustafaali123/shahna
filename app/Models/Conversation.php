<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Conversation
 * 
 * @property int $id
 * @property int $sender_id
 * @property int $receiver_id
 * @property string|null $title
 * @property int $sender_viewed
 * @property int $receiver_viewed
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class Conversation extends Model
{
	protected $table = 'conversations';

	protected $casts = [
		'sender_id' => 'int',
		'receiver_id' => 'int',
		'sender_viewed' => 'int',
		'receiver_viewed' => 'int'
	];

	protected $fillable = [
		'sender_id',
		'receiver_id',
		'title',
		'sender_viewed',
		'receiver_viewed'
	];
}
